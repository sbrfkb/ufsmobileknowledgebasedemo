export const authorize = () => ({
    type: 'AUTHORIZED',
    payload: {
        show: 'knowledgeBase'
    }
});