// Первоначальное состояние: мы не залогинены
const initialState = {
    show: 'login'
};

const loginReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'AUTHORIZED':
            return {
                ...state,
                show: action.payload.show
            };

        default:
            return state;
    }
};

export default loginReducer;