import React, {Component} from 'react';
import {View} from 'ufs-mobile-platform';
import Login from "./login";
import AfterLogin from './afterlogin.js';

const routes = {
  login: Login,
  knowledgeBase: AfterLogin
};

export default class RouterApp extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const ActiveApp = routes[this.props.show];
        return(!routes[this.props.show]
            ? <View />
            : <ActiveApp {...this.props} />
        )
    }
}

