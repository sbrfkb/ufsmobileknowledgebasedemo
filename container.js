import React, {Component} from 'react';
import RouterApp from './routerapp';
import {authorize} from './src/actions';
import {connect} from 'react-redux';

class Container extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return(
            <RouterApp {...this.props} />
        )
    }
}

const mapStateToProps = state => ({
    show: state.user.loginReducer.show
});

const mapDispatchToProps = dispatch => ({authorize: () => dispatch(authorize())});

export default connect(mapStateToProps, mapDispatchToProps)(Container);