import React, {Component} from 'react';
import {AppContainer, Content, IconType, MenuItem, MenuSideTop, RootComponent, Sidebar} from 'ufs-mobile-platform';
import {StatusBar} from 'react-native';
import Container from './container';


StatusBar.setBarStyle('light-content', false);


export default class App extends Component {
    constructor(props) {
        super(props);
    }

  render() {
    return (
      <RootComponent>
        <Sidebar>
          <MenuSideTop>
            <MenuItem index={0} iconType={IconType.Library}/>
          </MenuSideTop>
        </Sidebar>
        <Content>
          <AppContainer content={contentsLoader(0)} />
        </Content>
      </RootComponent>
    );
  }
}

const contentsLoader = (page) => {
  let scene;

  switch (page) {
    case 0:
      scene = Container;
      break;
  }

  return new Promise(function (resolve) {
    resolve(scene)
  });
};
