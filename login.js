import React, {Component} from 'react';
import { Alert } from 'react-native';
import { AuthForm, post as UFSPost } from 'ufs-mobile-platform';
import base64 from 'base-64';
import {BASE_URL, RB_PROJECT_ID, KB_PROJECT_ID} from './src/constants';


export default class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            login: 'testcrmmanager8',
            password: 'q12345678',
            isLoading: false
        }
    }


    sendRequest = (url) => {
        let requestBody = {"traverseTree": {"target": "/", "depth": 99}};
        let requestHeaders = {
            'Authorization': 'Basic ' + base64.encode(`${this.state.login}-ext:${this.state.password}`),
            'Content-type': 'application/json'
        };
        UFSPost(url, requestBody, requestHeaders)
            .then(response => {
                if (response.success === true) {
                    this.props.authorize();
                } else {
                    console.log('no response, status: ', response);
                    this.setState({
                        isLoading: false
                    });
                }
            })
            .catch(e => {
                console.log(e);
                Alert.alert('Авторизация не удалась: ' + e);
            });

        this.setState({
            login: '',
            password: '',
            isLoading: true
        });
    };

    render() {
        return (<AuthForm
            loading={this.state.isLoading}
            login={ this.state.login }
            password={ this.state.password }
            onLoginChange={ (login) => (this.setState({ login })) }
            onPasswordChange={ (password) => (this.setState({ password })) }
            onLoginClick={ () => (this.sendRequest(`${BASE_URL}/tree/${RB_PROJECT_ID}`))}
        />);
    }
}