import React, {Component} from 'react';
import {UFSKnowledgeBaseContainer} from 'ufs-mobile-knowledge-base';

export default class AfterLogin extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return(<UFSKnowledgeBaseContainer {...this.props} />
        )
    }
}

