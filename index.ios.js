import React, {Component} from 'react';
import {AppRegistry} from 'react-native';
import {UFSProvider} from 'ufs-mobile-platform';
import App from './App';
import {createLogger} from 'redux-logger';
import {combineReducers} from 'redux';
import {setupConfig} from 'ufs-mobile-knowledge-base';
import loginReducer from './src/reducers';
import {BASE_URL, TFS_BASE_URL, TFS_BASE_DOWNLOAD_URL, SCENARIO_ID, TARGET, FOLDER_TARGET, ECM_USER_ID, UFS_USER_ID, RB_PROJECT_ID, KB_PROJECT_ID, LOG_FILE_TTL, LOG_STORE_URL, LOG_FILE_MAX_SIZE, EXTERNAL_UZ_SUDIR} from './src/constants';

export const config = {
    baseURL: BASE_URL,
    ecmConfig: {
        downloadURL: `${TFS_BASE_URL}/file/download`,
        statusURL: `${TFS_BASE_URL}/file/status`,
        fileURL: `${TFS_BASE_DOWNLOAD_URL}/kb-file/`,
        maxRetryCount: 30,
        pollingTimeout: 4,
        cachePolicy: {
            type: 'DIRECT',
            maxTTL: 5
        }
    },
    user: ECM_USER_ID,
    scenarioId: SCENARIO_ID,
    target: TARGET,
    projectID: KB_PROJECT_ID,
    logConfig:{
        'logFileTTL': LOG_FILE_TTL,
        'logStoreURL': LOG_STORE_URL,
        'logFileMaxSize': LOG_FILE_MAX_SIZE,
        'externalUZSUDIR': EXTERNAL_UZ_SUDIR
    }
};

setupConfig(config);

import {reducers} from 'ufs-mobile-knowledge-base';

const app = combineReducers({
    ...reducers,
    loginReducer
});


export default class UFSMobileKnowledgeBaseDemo extends Component {
  render() {
    return (
      <UFSProvider reducer={app} middleware={[createLogger()]}>
        <App/>
      </UFSProvider>
    );
  }
}

AppRegistry.registerComponent('UFSMobileKnowledgeBaseDemo', () => UFSMobileKnowledgeBaseDemo);
