#import "AppDelegate.h"
#import <UFSLibrary/UFSMobilePlatform.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>


@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [UFSMobilePlatform initializeWithAppDelegate:self
                                   launchOptions:launchOptions
                                         devMode:YES];
    [Fabric with:@[[Crashlytics class]]];

    return YES;
}

@end
